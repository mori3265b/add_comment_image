Attribute VB_Name = "Module1"
Sub add_image_comment_raw_size()
    
    If TypeName(ActiveCell.Comment) = "Comment" Then Exit Sub
    
    Dim vntFileName As String
    vntFileName = Application.GetOpenFilename(Title:="select image", MultiSelect:=False)
    If Len(vntFileName) <= 0 Then Exit Sub
    With CreateObject("Scripting.FileSystemObject")
        If Not LCase(.GetExtensionName(vntFileName)) = "jpg" Then Exit Sub
    End With
    
    Dim IMG As Object
    Set IMG = LoadPicture(vntFileName)
    With ActiveCell.AddComment
        .Shape.Fill.UserPicture vntFileName
        .Shape.Height = Application.CentimetersToPoints(IMG.Height) / 1000
        .Shape.Width = Application.CentimetersToPoints(IMG.Width) / 1000
        .Shape.Shadow.Visible = msoFalse
        .Shape.Line.Visible = msoFalse
        .Visible = True
    End With
End Sub
